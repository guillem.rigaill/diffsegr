
<!-- README.md is generated from README.Rmd. Please edit that file -->

# DiffSegR

<!-- badges: start -->
<!-- badges: end -->

## Installation

You can install the development version from
[GitLab](https://forgemia.inra.fr/) with:

``` r
# install.packages("devtools")
devtools::install_git(
  url             = "https://forgemia.inra.fr/guillem.rigaill/diffsegr",
  build_vignettes = TRUE
)
```

## Getting started

For an illustration of *how to use* DiffSegR check the available
vignette.

``` r
vignette("DiffSegR")
```
